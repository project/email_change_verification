CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

This module adds email verification to the user accounts when changing their email.

When a user changes their email address, this module will temporarily save the new value and check if it is already used. If it is not already used, it will send a verification email (configurable) to the new email address with a one-time verification link. Upon using that link the logged in user's email address will be changed to the temporarily stored new address, provided that they are the correct currently logged in user.

If a user wants to change their email address to an email address that is already in use, an optional separately configured email will be sent to the email address (e.g. "Someone has attempted to use this email address for another account on site.org").

This module prevents malicious attempts to gather email addresses with active accounts by trying to change to them.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/email_change_verification

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

* Enable and configure at /admin/config/people/accounts/email-change-verification

MAINTAINERS
-----------

Current maintainers:
* Laurens Van Damme (L_VanDamme) - https://www.drupal.org/u/l_vandamme
* Alex Kuzava (nginex) - https://www.drupal.org/u/nginex
* Dieter Blomme (daften) - https://www.drupal.org/u/daften
