<?php

namespace Drupal\email_change_verification\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures email_change_verification settings.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'email_change_verification_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'email_change_verification.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('email_change_verification.config');

    // Ensure there is config to prevent errors.
    if (empty($config->getRawData())) {
      $config->setData([]);
      $config->save();
    }

    // Verification email fields.
    $form['email_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Verification email'),
    ];
    $form['email_wrapper']['verification_mail_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Verification mail subject'),
      '#description' => $this->t('Enter the subject for the verification mail sent when a user requests an email address change.'),
      '#default_value' => $config->get('verification_mail_subject'),
    ];
    $default_body = $config->get('verification_mail_body');
    $default_body = !is_array($default_body) ? [] : $default_body;
    $form['email_wrapper']['verification_mail_body'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Verification mail body'),
      '#description' => $this->t('Enter the body for the verification mail sent when a user requests an email address change.'),
      '#default_value' => !empty($default_body['value']) ? $default_body['value'] : '',
      '#format' => !empty($default_body['format']) ? $default_body['format'] : 'full_html',
    ];

    // Message fields.
    $form['message_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Message'),
    ];
    $form['message_wrapper']['show_message'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show message'),
      '#description' => $this->t('Enable to show a configurable message when a user changes their email address.'),
      '#default_value' => $config->get('show_message'),
    ];
    $default_message = $config->get('message');
    $default_message = !is_array($default_message) ? [] : $default_message;
    $form['message_wrapper']['message'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Message'),
      '#description' => $this->t('Enter the message shown when a user requests an email address change.'),
      '#default_value' => !empty($default_message['value']) ? $default_message['value'] : '',
      '#format' => !empty($default_message['format']) ? $default_message['format'] : 'full_html',
      '#states' => [
        'visible' => [
          'input[name="show_message"]' => ['checked' => TRUE],
        ],
      ],
    ];

    // Add a token browser.
    $form['token_browser'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => ['site', 'user'],
      '#click_insert' => TRUE,
      '#dialog' => TRUE,
    ];

    // Timeout field.
    $form['timeout_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Verification timeout'),
    ];
    $form['timeout_wrapper']['timeout'] = [
      '#type' => 'select',
      '#title' => $this->t('Timeout'),
      '#description' => $this->t('Select the amount of time a verification link stays valid.'),
      '#options' => [
        14400 => '4 ' . $this->t('hours'),
        28800 => '8 ' . $this->t('hours'),
        57600 => '16 ' . $this->t('hours'),
        86400 => '1 ' . $this->t('day'),
        172800 => '2 ' . $this->t('days'),
        604800 => '7 ' . $this->t('days'),
        1209600 => '14 ' . $this->t('days'),
        2592000 => '30 ' . $this->t('days'),
      ],
      '#default_value' => $config->get('timeout'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('email_change_verification.config')
      ->set('verification_mail_subject', $values['verification_mail_subject'])
      ->set('verification_mail_body', $values['verification_mail_body'])
      ->set('show_message', $values['show_message'])
      ->set('message', $values['message'])
      ->set('timeout', $values['timeout'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
