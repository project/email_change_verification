<?php

namespace Drupal\email_change_verification\Event;

/**
 * Define email change verification events.
 */
final class EmailChangeVerificationEvents {

  const EMAIL_CHANGE_VERIFICATION_SET_REDIRECT = 'email_change_verification.set_redirect';

  const EMAIL_CHANGE_VERIFICATION_SUCCESS = 'email_change_verification.success';

}
