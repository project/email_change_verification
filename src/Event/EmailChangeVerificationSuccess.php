<?php

namespace Drupal\email_change_verification\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\user\UserInterface;

/**
 * Event that is fired when a user successfully verifies an email change.
 *
 * Indicates that the user's new email address is valid.
 */
class EmailChangeVerificationSuccess extends Event {

  /**
   * The account for which the email was successful validated.
   *
   * @var \Drupal\user\UserInterface
   */
  public $account;

  /**
   * VerifiedUserEmailChange constructor.
   *
   * @param \Drupal\user\UserInterface $account
   *   The current user.
   */
  public function __construct(UserInterface $account) {
    $this->account = $account;
  }

  /**
   * Get the account.
   *
   * @return \Drupal\user\UserInterface
   *   The current user.
   */
  public function getAccount(): UserInterface {
    return $this->account;
  }

}
