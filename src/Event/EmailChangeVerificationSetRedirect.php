<?php

namespace Drupal\email_change_verification\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\user\UserInterface;

/**
 * Allow developers to set the redirect destination.
 *
 * This event alters the passed-in route variables instead of returning a
 * RedirectResponse or similar object because those currently get fired
 * immediately if returned by an event.
 */
class EmailChangeVerificationSetRedirect extends Event {

  /**
   * The user account for which this change is being made.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $account;

  /**
   * The route name of the redirect to be set.
   *
   * @var string
   */
  protected $routeName;

  /**
   * The parameters required by the route to be set.
   *
   * @var array
   */
  protected $routeParameters;

  /**
   * Any additional options to accompany the route.
   *
   * @var array
   */
  protected $options;

  /**
   * EmailChangeVerificationSetRedirect constructor.
   *
   * @param \Drupal\user\UserInterface $account
   *   The current user.
   * @param string $route_name
   *   The route name.
   * @param array $route_parameters
   *   The route params.
   * @param array $options
   *   The route options.
   */
  public function __construct(UserInterface $account, string $route_name, array $route_parameters = [], array $options = []) {
    $this->account = $account;
    $this->routeName = $route_name;
    $this->routeParameters = $route_parameters;
    $this->options = $options;
  }

  /**
   * Get the user account object.
   *
   * @return \Drupal\user\UserInterface
   *   The current user object.
   */
  public function getAccount(): UserInterface {
    return $this->account;
  }

  /**
   * Set the route name.
   *
   * @param string $route_name
   *   The route name.
   */
  public function setRouteName(string $route_name): void {
    $this->routeName = $route_name;
  }

  /**
   * Get the route name.
   *
   * @return string
   *   The route name.
   */
  public function getRouteName(): string {
    return $this->routeName;
  }

  /**
   * Set the route params.
   *
   * @param array $route_parameters
   *   The route params.
   */
  public function setRouteParameters(array $route_parameters): void {
    $this->routeParameters = $route_parameters;
  }

  /**
   * Get the route params.
   *
   * @return array
   *   The route params.
   */
  public function getRouteParameters(): array {
    return $this->routeParameters;
  }

  /**
   * Set the route options.
   *
   * @param array $options
   *   The route options.
   */
  public function setOptions(array $options): void {
    $this->options = $options;
  }

  /**
   * Get the route options.
   *
   * @return array
   *   The route options.
   */
  public function getOptions(): array {
    return $this->options;
  }

}
