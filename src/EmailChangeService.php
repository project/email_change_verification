<?php

namespace Drupal\email_change_verification;

use Drupal\Core\Url;
use Drupal\Core\Render\Markup;
use Drupal\Core\Site\Settings;
use Drupal\Core\Utility\Token;
use Drupal\user\UserInterface;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Database\Connection;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * A utility service for the Email change verification module.
 *
 * Supplies functions for generating verification links and updating accounts.
 *
 * @package Drupal\beobank_apply_forms
 */
class EmailChangeService implements EmailChangeServiceInterface {

  /**
   * The Drupal database connection service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The Email change verification configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The Drupal email manager service.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * The Drupal messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The Drupal token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $tokenService;

  /**
   * The Drupal language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * EmailChangeService constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The Drupal database connection service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The Drupal configuration factory.
   * @param \Drupal\Core\Mail\MailManagerInterface $mailManager
   *   The Drupal email manager service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The Drupal messenger service.
   * @param \Drupal\Core\Utility\Token $tokenService
   *   The Drupal token service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The Drupal language manager.
   */
  public function __construct(
    Connection $database,
    ConfigFactoryInterface $configFactory,
    MailManagerInterface $mailManager,
    MessengerInterface $messenger,
    Token $tokenService,
    LanguageManagerInterface $languageManager) {

    $this->database = $database;
    $this->config = $configFactory->get('email_change_verification.config');
    $this->mailManager = $mailManager;
    $this->messenger = $messenger;
    $this->tokenService = $tokenService;
    $this->languageManager = $languageManager;
  }

  /**
   * Process an email change request.
   *
   * @param \Drupal\user\UserInterface $account
   *   The user account for which the email change was requested.
   * @param string $newEmail
   *   The newly requested email address.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function changeRequest(UserInterface $account, $newEmail) {
    // Update the user's mail_change field.
    $account->set('mail_change', $newEmail);
    $account->save();

    // Set up and send the verification email.
    $params = [
      'subject' => $this->tokenService->replace($this->config->get('verification_mail_subject'), ['user' => $account]),
      'body' => $this->tokenService->replace(
        $this->config->get('verification_mail_body')['value'], ['user' => $account]
      ),
    ];
    $this->mailManager->mail(
      'email_change_verification',
      'verification_mail',
      $newEmail,
      $this->languageManager->getCurrentLanguage()->getId(),
      $params
    );

    // Set a message if configured.
    if ($this->config->get('show_message')) {
      $this->messenger->addMessage(
        Markup::create($this->tokenService->replace($this->config->get('message')['value'], ['user' => $account]))
      );
    }
  }

  /**
   * Generates a verification URL.
   *
   * @param \Drupal\user\UserInterface $account
   *   The user account for which the email change was requested.
   *
   * @return \Drupal\Core\Url|bool
   *   The verification URL or false.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function generateVerificationUrl(UserInterface $account) {
    $mailChange = $account->get('mail_change');
    if (!$mailChange->isEmpty()) {
      $newEmail = $mailChange->first()->getValue()['value'];
      $timestamp = time();
      $url_options = [
        'absolute' => TRUE,
        'language' => $this->languageManager->getCurrentLanguage(),
      ];
      return Url::fromRoute('email_change_verification.mail_change', [
        'user' => $account->id(),
        'timestamp' => $timestamp,
        'new_email' => $newEmail,
        'hash' => $this->getHash($account, $timestamp, $newEmail),
      ], $url_options);
    }
    else {
      return FALSE;
    }
  }

  /**
   * Checks a verification hash.
   *
   * @param \Drupal\user\UserInterface $account
   *   The user account for which the email change was requested.
   * @param string $new_email
   *   The newly requested email address.
   * @param int $timestamp
   *   The timestamp of the email change request.
   * @param string $hash
   *   The hash to check.
   *
   * @return bool
   *   Boolean indicating whether the has is correct or not.
   */
  public function checkHash(UserInterface $account, $new_email, $timestamp, $hash) {
    $new_hash = $this->getHash($account, $timestamp, $new_email);
    return hash_equals($hash, $new_hash);
  }

  /**
   * Helper function to get a hash to use in the verification URL.
   *
   * @param \Drupal\user\UserInterface $account
   *   The user account for which the email change was requested.
   * @param int $timestamp
   *   The timestamp of the email change request.
   * @param string $newEmail
   *   The newly requested email address.
   *
   * @return string
   *   The hash.
   */
  protected function getHash(UserInterface $account, $timestamp, $newEmail) {
    $data = $timestamp;
    $data .= $account->id();
    $data .= $account->getEmail();
    $data .= $newEmail;
    return Crypt::hmacBase64($data, Settings::getHashSalt() . $account->getPassword());
  }

  /**
   * Helper function to check if an email address is unique.
   *
   * @param string $mail
   *   The email address to check.
   *
   * @return bool
   *   Boolean indicating whether the email address is unique.
   */
  public function isUnique($mail) {
    $q = $this->database->select('users_field_data', 'u');
    $q->fields('u', ['uid']);
    $q->condition('u.mail', $mail);
    return !$q->execute()->fetchField();
  }

}
