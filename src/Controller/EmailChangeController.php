<?php

namespace Drupal\email_change_verification\Controller;

use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\email_change_verification\EmailChangeServiceInterface;
use Drupal\email_change_verification\Event\EmailChangeVerificationEvents;
use Drupal\email_change_verification\Event\EmailChangeVerificationSetRedirect;
use Drupal\email_change_verification\Event\EmailChangeVerificationSuccess;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MailChangeController.
 *
 * @package Drupal\email_change_verification\Controller
 */
class EmailChangeController extends ControllerBase {

  /**
   * The Email change service.
   *
   * @var \Drupal\email_change_verification\EmailChangeServiceInterface
   */
  protected $emailChangeService;

  /**
   * Symfony's event dispatcher.
   *
   * @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher
   */
  protected $eventDispatcher;

  /**
   * EmailChangeController constructor.
   *
   * @param \Drupal\email_change_verification\EmailChangeServiceInterface $emailChangeService
   *   The Email change service.
   * @param \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher $eventDispatcher
   *   Symfony's event dispatcher.
   */
  public function __construct(EmailChangeServiceInterface $emailChangeService, ContainerAwareEventDispatcher $eventDispatcher) {
    $this->emailChangeService = $emailChangeService;
    $this->eventDispatcher = $eventDispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('email_change_verification.service'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * Handles the email verification.
   *
   * In order to never disclose a mail change link via a referrer header this
   * controller must always return a redirect response.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user account for which the email change was requested.
   * @param string $new_email
   *   The newly requested email address.
   * @param int $timestamp
   *   The timestamp of the email change request.
   * @param string $hash
   *   Unique hash.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   An HTTP response doing a redirect.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function verify(UserInterface $user, $new_email, $timestamp, $hash) {
    // If the user is not logged in, redirect to the login page with
    // the correct destination.
    /** @var \Drupal\Core\Session\AccountProxyInterface $current_user */
    $current_user = $this->currentUser();
    if ($current_user->isAnonymous()) {
      $this->messenger()->addError($this->t('You must be logged in to verify your email address.'));
      return $this->redirect('user.login', [], ['query' => $this->getDestinationArray()]);
    }

    // This event changes these passed-in variables instead of returning a
    // RedirectResponse or similar object because those currently get fired
    // immediately if returned by an event.
    $route_name = '<front>';
    $route_parameters = [];
    $route_options = [];
    $setRedirectEvent = new EmailChangeVerificationSetRedirect($user, $route_name, $route_parameters, $route_options);
    $this->eventDispatcher->dispatch($setRedirectEvent, EmailChangeVerificationEvents::EMAIL_CHANGE_VERIFICATION_SET_REDIRECT);
    $redirect = $this->redirect($setRedirectEvent->getRouteName(), $setRedirectEvent->getRouteParameters(), $setRedirectEvent->getOptions());

    // If the current user does not equal the user for which to change the
    // address, throw an error.
    if ($current_user->id() !== $user->id()) {
      $this->messenger()->addError($this->t(
        'You are currently logged in as %user, and are attempting to verify an email address change for another account. Please <a href=":logout">log out</a> and try using the link again.',
        [
          '%user' => $current_user->getAccountName(),
          ':logout' => Url::fromRoute('user.logout')->toString(),
        ]
      ));
      return $redirect;
    }

    // If the link has expired, throw an error.
    $timeout = $this->config('email_change_verification.config')->get('timeout');
    if ($timestamp + $timeout < time()) {
      $this->messenger()->addError($this->t('You have tried to use an email address verification link that has expired. Please visit your account and change your email again.'));
      return $redirect;
    }

    // If the hash is not correct, throw an error.
    if (!$this->emailChangeService->checkHash($user, $new_email, $timestamp, $hash)) {
      $this->messenger()->addError($this->t('You have tried to use an email address verification link that has either been used or is no longer valid. Please visit your account and change your email again.'));
      return $redirect;
    }

    // If none of the above errors occurred, change the email address and to the
    // front page.
    if ($this->emailChangeService->isUnique($new_email)) {
      $user->setEmail($new_email);
      $user->set('mail_change', '');
      $user->save();
      $this->messenger()->addMessage($this->t('Your email address has been changed to %mail.', ['%mail' => $new_email]));
      $successEvent = new EmailChangeVerificationSuccess($user);
      $this->eventDispatcher->dispatch($successEvent, EmailChangeVerificationEvents::EMAIL_CHANGE_VERIFICATION_SUCCESS);
    }
    else {
      $this->messenger()->addError($this->t('You have tried to use an email address that is already in use.'));
    }
    return $redirect;
  }

}
