<?php

namespace Drupal\email_change_verification;

use Drupal\user\UserInterface;

/**
 * A utility service for the Email change verification module.
 *
 * Supplies functions for generating verification links and updating accounts.
 *
 * @package Drupal\beobank_apply_forms
 */
interface EmailChangeServiceInterface {

  /**
   * Process an email change request.
   *
   * @param \Drupal\user\UserInterface $account
   *   The user account for which the email change was requested.
   * @param string $newEmail
   *   The newly requested email address.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function changeRequest(UserInterface $account, $newEmail);

  /**
   * Generates a verification URL.
   *
   * @param \Drupal\user\UserInterface $account
   *   The user account for which the email change was requested.
   *
   * @return \Drupal\Core\Url|bool
   *   The verification URL or false.
   */
  public function generateVerificationUrl(UserInterface $account);

  /**
   * Checks a verification hash.
   *
   * @param \Drupal\user\UserInterface $account
   *   The user account for which the email change was requested.
   * @param string $new_email
   *   The newly requested email address.
   * @param int $timestamp
   *   The timestamp of the email change request.
   * @param string $hash
   *   The hash to check.
   *
   * @return bool
   *   Boolean indicating whether the has is correct or not.
   */
  public function checkHash(UserInterface $account, $new_email, $timestamp, $hash);

  /**
   * Helper function to check if an email address is unique.
   *
   * @param string $mail
   *   The email address to check.
   *
   * @return bool
   *   Boolean indicating whether the email address is unique.
   */
  public function isUnique($mail);

}
