<?php

/**
 * @file
 * This file is used to write hooks that used in the module.
 */

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function email_change_verification_token_info() {
  return [
    'tokens' => [
      'user' => [
        'email_change_verification_link' => [
          'name' => t('Email change verification link'),
          'description' => t('The verification link a user must access in order to complete the email address change.'),
        ],
        'mail_change' => [
          'name' => t('Mail change'),
          'description' => t('The email address the user requested a change to.'),
        ],
      ],
    ],
  ];
}

/**
 * Implements hook_tokens().
 */
function email_change_verification_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];

  if ($type == 'user' && !empty($data['user'])) {
    /** @var \Drupal\user\UserInterface $account */
    $account = $data['user'];
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'email_change_verification_link':
          /** @var \Drupal\email_change_verification\EmailChangeServiceInterface $service */
          $service = Drupal::service('email_change_verification.service');
          $url = $service->generateVerificationUrl($account);
          $replacements[$original] = !empty($url) ? $url->toString() : '';
          break;

        case 'mail_change':
          $mailChange = $account->get('mail_change');
          if (!$mailChange->isEmpty()) {
            $replacements[$original] = $mailChange->first()->getValue()['value'];
          }
          break;
      }
    }
  }

  return $replacements;
}
